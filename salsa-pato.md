# Salsa pató.

Salsa base para muchos platos diferentes. Para todo -> pató. No incluye pato.

## Ingredientes para 6 porciones.

- Tomate. 1 kg.
- Pimiento rojo o verde. 1 grande.
- Ajo. 1 cabesa.
- Cebolla. 2 grandes.
- Aceite de oliva. 1 chorrito.
- Sal. Al gusto.
- Pimienta negra molida. Al gusto.
- Pimienta negra en grano. Al gusto.
- Laurel. 1 hoja.
- Clavo. 2 unidades.


## Preparación

1. Picar tomates, pimiento, ajo y cebollas en trocitos, y poner todo en un caldero grande junto con el resto de ingredientes.

2. Cocinar a fuego medio/bajo durante 30 minutos.

3. Quitar la hoja de laurel y moler en el mismo caldero con la batidora de mano.

4. Dejar enfiar un poco.

5. Verter la salsa en 6 recipientes herméticos y congelar.
