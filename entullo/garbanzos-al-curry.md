# Garbanzos al curry

Garbanzos al curry. Acompañar con arroz blanco.

Cantidad: 4 platos.

Tiempo de preparación: 20 min.
Tiempo de cocción: 30 min.
Tiempo total: 50 min.

## Ingredientes

- **Tomate.** 2 medianos, en trocitos
- **Cebolla.** 1 mediana, en trocitos
- **Sal.** al gusto
- **Pimienta negra molida.** al gusto
- **Garbanzos.** Un bote de 400 g
- **Ajo.** 2 dientes, en rodajitas
- **Garam masala.** 1.5 cucharadas
- **Curry.** 1/2 cucharada
- **Comino.** 1/4 cucharada
- **Pimienta picona**. 1/2 pequeña [Opcional]
- **Leche de coco.** 1 lata de 400 ml
- **Coriander.** 1 small bunch
- **Lime or lemon.** 1/4 per person


## Preparación

1. Poner aceite en un caldero y cuando esté caliente añadir la cebolla, el tomate, sal y pimienta. Sofreír a fuego medio durante 10 minutos, hasta que la cebolla esté blanda y el tomate haya soltado su jugo.

2. Añadir garbanzos, ajo y especias. Revolver.

3. A continuación añadir la leche de coco y revolver. Llevar a ebullición.

4. Cuando bulla, bajar a fuego medio y cocinar a fuego lento durante 10 minutos.

5. Apartar del fuego y añadir el cilantro picado por encima, sin revolver.

6. Después de servir en el plato, exprimir unas gotas de lima por encima.

7. Acompañar de arroz blanco, preferiblemente basmati.
