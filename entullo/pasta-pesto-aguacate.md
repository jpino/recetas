# Pasta con pesto de aguacates

Pasta con pesto de aguacates.

## Ingredientes para 4 raciones

- Pasta. 400 g.

### Pesto

- Aguacate. 2 medianos.
- Queso rallado vegano. 100 g.
- Ajo. 5 dientes.
- Albahaca. 5 hojas grandes.
- Cilantro (opcional). 1 manojito.
- Nueces (o cualquier otro fruto seco) (opcional). 1 puñadito.
- Lima. Jugo de 1/2 lima.
- Sal. Al gusto.
- Pimienta. Al gusto.
- Agua. Muy poquita.

### Adorno

- Tomates cherry (opcional). 12 unidades cortadas a la mitad.


## Preparación

- Poner la pasta a hervir.

- Preparar los ingredientes del pesto y añadirlos al vaso de la batidora de mano. Batir hasta que quede una mezcla bastante homogénea. Si es necesario, ir añadiendo agua poco a poco para facilitar la mezcla.

- Cuando la pasta esté hecha, escurrir el agua, añadir el pesto al caldero y mezclar bien.

- Calentar la pasta con el pesto durante un minuto, evitando que se pegue al fondo.

- Opcional: Servir y adornar el plato con tomates cherry.

