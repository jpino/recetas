# Mousse de chocolate con base de aguacate

Mousse de chocolate con base de aguacate que no sabe a aguacate sino a chocolate. Un misterio.

## Ingredientes para 6 porciones.

- **Aguacate.** 2 medianos
- **Cacao en polvo sin azúcar.** 1/4 de vaso.
- **Chocolate puro en tableta.** 50 g.
- **Leche vegetal.** 1/4 de vaso.
- **Sirope de agave (o similar).** 1/4 de vaso.
- **Jugo de limón.** 1/8 de vaso.
- **Extracto de vainilla.** 1 cucharada.
- **Sal.** Una pizca.
- **Frutos rojos (opcional).** Unos poquitos.
- **Frutos secos (opcional).** Unos poquitos, troceados.

## Preparación

1. En un vaso añadir la leche y el chocolate en tableta troceado y calentar en el microondas 30 segundos (o hasta que derrita).

2. Añadir al vaso de la batidora los aguacates troceados, cacao en polvo, sirope de agave, extracto de vainilla, y la pizca de sal.

3. Batir bien hasta conseguir una mezcla homogénea. Añadir leche si es necesario para facilitar la mezcla.

4. Verter la mezcla en 6 recipientes pequeños y adornar con frutos rojos y frutos secos.

5. Enfriar en la nevera al menos una hora antes de comer.

